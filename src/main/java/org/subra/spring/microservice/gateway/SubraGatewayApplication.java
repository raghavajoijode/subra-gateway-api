package org.subra.spring.microservice.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SubraGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(SubraGatewayApplication.class, args);
	}

}
